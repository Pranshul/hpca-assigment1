#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <fstream>
using namespace std;

#define FILTER_SIZE 3
#define FILTERS 2

// Optimize this function
void applyFilters(int *image, int height, int width, int *filter, int filters)
{
    for(int i = 0; i < filters; ++i)
    {
        int *copy = new int[height * width];
        
        for(int h = 0; h < height; ++h)
        {
            for(int w = 0; w < width; ++w)
            {
                copy[h * width + w] = 0;
                
                
                
                if(h == 0 && w == 0){copy[h * width + w] = (image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ]) + (image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ])+(image[(h+1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]);}
                else if(h == 0 && w == width-1){copy[h * width + w] = (image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ]) +(image[(h+1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE ])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ]);}
                else if(h == 0 ){copy[h * width + w] = (image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ]) + (image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2])+(image[(h+1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE ])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ])+(image[(h+1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]);}


                        else if(h == height-1 && w == 0)
                            {copy[h * width + w] = (image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h-1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 2])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ])+(image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2]);}
                            else if(h == height-1 && w == width-1)
                            {copy[h * width + w] = (image[(h-1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE])+(image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ]);}
                            else if(h == height-1)
                            {copy[h * width + w] = (image[(h-1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE])+(image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h-1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 2])+(image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ])+(image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2]);}

                        else if(w == 0)
                            {copy[h * width + w] = (image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h-1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 2])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ])+(image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ])+(image[(h+1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]);}

                        else if(w == width-1)
                            {copy[h * width + w] = (image[(h-1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE])+(image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ])+(image[(h+1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE ])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ]);}

			else
                        copy[h * width + w] += (image[(h-1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE])+(image[(h-1) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 1 ])+(image[(h-1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (0) * FILTER_SIZE + 2])+(image[(h) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE ])+(image[(h) * width +  w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 1 ])+(image[(h) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (1) * FILTER_SIZE + 2])+(image[(h+1) * width + (-1) + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE ])+(image[(h+1) * width + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 1 ])+(image[(h+1) * width + 1 + w] * filter[i * FILTER_SIZE * FILTER_SIZE + (2) * FILTER_SIZE + 2]);
                
                
                
                
               //if(h>1 && w>1 )
              // image[(h-2) * width + w-2] = copy[(h-2) * width + w-2];
                
            }
        }
        
		for(int h = height-1; h >-1; --h)
        {
            for(int w = width -1; w >-1; --w)
            	{
                image[h * width + w] = copy[h * width + w];}
        }
        
        delete copy;
    }
}


int main()
{
    int ht, wd;
    cin >> ht >> wd;
    int *img = new int[ht * wd];
    for(int i = 0; i < ht; ++i)
        for(int j = 0; j < wd; ++j)
            cin >> img[i * wd + j];
            
    int filters = FILTERS;
    int *filter = new int[filters * FILTER_SIZE * FILTER_SIZE];
    for(int i = 0; i < filters; ++i)
        for(int j = 0; j < FILTER_SIZE; ++j)
            for(int k = 0; k < FILTER_SIZE; ++k)
                cin >> filter[i * FILTER_SIZE * FILTER_SIZE + j * FILTER_SIZE + k];
    
    clock_t begin = clock();
    applyFilters(img, ht, wd, filter, filters);
    clock_t end = clock();
    cout << "Execution time: " << double(end - begin) / (double)CLOCKS_PER_SEC << " seconds\n";
    ofstream fout("outputt.txt");
    for(int i = 0; i < ht; ++i)
    {
        for(int j = 0; j < wd; ++j)
            fout << img[i * wd + j] << " ";
        fout << "\n";
    }
}
